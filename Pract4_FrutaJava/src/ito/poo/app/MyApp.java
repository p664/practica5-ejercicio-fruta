package ito.poo.app;

import Pract4_FrutaUML.Fruta;
import Pract4_FrutaUML.Periodo;

public class MyApp {

	public static void main(String[] args) {
		
		Fruta f=new Fruta("Durazno", 92.30f, 537.93f, 1930.60f);
	    System.out.println(f);
	    
	    Periodo p=new Periodo("5 meses", 530.00f);
	    f.AgregarPeriodo("6 meses", 1930.67f);
	    f.CostoPeriodo(5);
	    f.GananciaEstimada(2);
	    f.EliminarPeriodo(1);
	    System.out.println(f);
	}
}
